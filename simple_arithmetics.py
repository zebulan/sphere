f = '''\
4
12345+67890
324-111
325*4405
1234*4'''.split('\n')
from sys import stdin

for line in stdin:
    current = line.rstrip()
    if not current.isdigit():         # skips the first digit
        single_line = True
        op_index = 0
        for i, char in enumerate(current):
            if not char.isdigit():
                op_index = i          # index of operator (+, -, *)
                break

        answer = str(eval(current))
        left = current[:op_index]     # left of operator
        right = current[op_index+1:]  # right of operator
        op = current[op_index]        # operator (+, -, *)

        a_len = len(answer)
        l_len = len(left)
        r_len = len(right) + 1        # length of right, +1 for operator

        if op == '*':                 # multiplication
            l_num = int(left)         # saves repeated calls below
            nums = [str(int(a) * l_num) for a in right[::-1]]
            if not len(nums) == 1:    # dual horizontal lines

# For each multiplication, multiply the first number by each digit
# of the second number. Put the partial results one below the other,
# starting with the product of the last digit of the second number.
# Each partial result should be aligned with the corresponding digit.
# That means the last digit of the partial product must be in the
# same column as the digit of the second number. No product may begin
# with any additional zeros. If a�particular digit is zero, the
# product has exactly one digit -- zero. If the second number has
# more than one digit, print another horizontal line under the partial
# results, and then print the sum of them.

# Below only works if the second partial result is a single digit <---

                single_line = False
                first_line = max(l_len, r_len)
                length = max(first_line, max(len(b) for b in nums), a_len)
                print('{:>{}}'.format(left, length))
                print('{:>{}}'.format(op+right, length))
                print('{:>{}}\n{:>{}}'.format('-' * first_line, length,
                                              nums[0], length))
                for index, val in enumerate(nums[1:], start=1):
                    print('{:>{}}'.format(val, length - index))
                print('{}\n{:>{}}\n'.format('-' * length, answer, length))

        if single_line:  # addition, subtraction and single line multiplication
            length = max(l_len, r_len, a_len)
            print('{:>{}}'.format(left, length))
            print('{:>{}}'.format(op+right, length))
            print('{}\n{:>{}}'.format('-' * length, answer, length))
            print()
